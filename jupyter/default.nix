{ jupyterWith # path obtained from fetchFromGitHub
, haskellPackages
, haskellPkg ? (_: [])
, pythonPkg ? (_: [])
, pythonOverridesList ? (_: _: [])
}:

let
  jupyterSrc = jupyterWith + "/nix";
  inherit (import jupyterSrc {}) pkgs lib;
  jupyterPython3 = (import jupyterSrc {}).python3;
  jupyter = import jupyterWith {};

  # iPython
  #
  # Uncomment this attributes for adding iPython and its package
  # into existing jupyter notebook
  #
  iPython = jupyter.kernels.iPythonWith {
      python3 =
        jupyterPython3.override (old: with lib; {
          packageOverrides =
            foldr composeExtensions (_: _: {})
              [  (old.packageOverrides or (_: _: {}))
                 pythonOverridesList
              ];
        });
      name = "iPython";
      packages = pythonPkg;
  };
  # iHaskell
  #
  # all haskell package contained in iHaskell will always the same
  # as packages used by obelisk.
  #
  # all haskellPackages
  iHaskellWithPackages = jupyter.kernels.iHaskellWith {
    inherit haskellPackages;
      name = "iHaskell";
      packages = haskellPkg;
  };
in {
  # Jupyter Lab and Kernels
  jupyterLabWithKernels =
    jupyter.jupyterlabWith {
      kernels =
        [
          iHaskellWithPackages
          iPython
        ];
    };
}
