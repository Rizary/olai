# OLAI (Obelisk Loves AI)

Key idea: To have jupyter lab ready when using `ob shell` in your project. Both package in jupyter lab and obelisk project are the same.

to try, run `nix-shell olai-example.nix -A shells.ghc` and start the jupyter by running `jupyter lab`. both frontend and backend left as is.

TODO:
- Write some example that uses hasktorch to modeling the data and served to reflex-dom
- Write some example that uses hvega to display charts to the frontend
- Put subtitle, version icon, license icon
- Gif's demo Data Science (Using hvega)
- Gif's demo Artificial Intelligence (hasktorch, pytorch backend)
- Features
- Get Started
  - Prerequisites
  - Installation (Windows, MacOS, Linux, NixOS)
  - Docker or VM
  - Setup
- Usage
- FAQ
- For Developers
  - Build
  - Test
  - Run
- Changelog
- Acknowledgements
