{ obelisk ? import ./. {}}:

  # This is the example on how obelisk project is structured.
  # Currently it has IPython and IHaskell kernels to be used inside jupyter lab.
  #
  # Notes:
  #   - This package import obelisk from this repository. In a separate project, the obelisk should be pointed
  #     into `.obelisk/impl/` and the `.obelisk/impl/github.json` should be pointed into this repository
  #   - For IPython, do not forget to change the kernels into IPython in the upper right corner of your jupyter lab
  #   - Please refer to
  #
obelisk.project ./. ({ pkgs, hackGet, ... }: {
      packages = {
        backend = ./backend;
        frontend = ./frontend;
        common = ./common;
      };
      shellToolOverrides = ghc: super:
        let
          inherit (import deps/shared.nix {inherit hackGet;}) pytorch-src;
          # haskellPackages contained all haskell's package available inside obelisk project
          # please refer to haskellOverrides in `./default.nix` if adding haskell package
          # is needed. To make new package set available inside jupyter lab, put it into
          # overrides attributes inside obelisk.project.
          haskellPackages = ghc;
          pythonPkg  =
            p: with p;
              [ pytorch numpy matplotlib pandas seaborn scikitlearn umap-learn ];
          haskellPkg =
            p: with p;
              [ hvega statistics vector ihaskell-hvega aeson aeson-pretty foldl
                hasktorch libtorch-ffi hasktorch-codegen HUnit random-shuffle
                rhyolite-aeson-orphans rhyolite-backend rhyolite-backend-db
                rhyolite-backend-snap rhyolite-common rhyolite-datastructures
                rhyolite-frontend rhyolite-logging reflex reflex-dom
              ];
          jupyter = obelisk.jupyter;
          pythonOverridesList =
            self: super: {
              numpy = super.numpy.override { blas = pkgs.mkl; };
              pytorch =
                (super.callPackage "${pytorch-src}/default.nix" {
                   mklSupport = false;
                   buildNamedTensor = true;
                   buildBinaries = true;
              });
            };
        in {
          inherit (jupyter haskellPackages haskellPkg pythonPkg pythonOverridesList) jupyterLabWithKernels;
          ghc-mod = null;
        };
  })
