import Backend
import Frontend
import Obelisk.Backend

import qualified Torch.Tensor as Tensor
import ATen.Cast

main :: IO ()
main = runBackend backend frontend
