import ((import <nixpkgs> {}).fetchFromGitHub (
  let json = builtins.fromJSON (builtins.readFile ./github.json);
  in { inherit (json) owner repo rev sha256 fetchSubmodules;
       private = json.private or false;
     }
))
