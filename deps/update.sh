nix-prefetch-git https://github.com/tweag/jupyterWith.git refs/heads/master --fetch-submodules --deepClone > jupyterWith/github.json
nix-prefetch-git https://github.com/DougBurke/hvega.git refs/heads/master --fetch-submodules --deepClone > haskell/hvega/github.json
nix-prefetch-git https://github.com/gibiansky/IHaskell.git refs/heads/master --fetch-submodules --deepClone > haskell/iHaskell/github.json
nix-prefetch-git https://github.com/hasktorch/hasktorch.git refs/heads/master --fetch-submodules --deepClone > haskell/hasktorch/github.json
nix-prefetch-git https://github.com/tensorflow/haskell.git refs/heads/master --fetch-submodules --deepClone > haskell/tensorflow/github.json
nix-prefetch-git https://github.com/stites/pytorch-world.git refs/heads/unstable --fetch-submodules --deepClone > python/pytorch/github.json



