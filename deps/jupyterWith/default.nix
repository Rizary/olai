import ((import <nixpkgs> {}).fetchFromGitHub (
  let json = builtins.fromJson (builtins.readFile ./github.json);
  in { inherit (json) owner repo rev sha256;
       private = json.private or false;
     }
))
