{ hackGet }:

let
  hvega = hackGet haskell/hvega + "/hvega";
  ihaskell-hvega = hackGet haskell/hvega + "/ihaskell-hvega";
  iHaskell = hackGet haskell/iHaskell;
  ghc-parser = hackGet haskell/iHaskell + "/ghc-parser";
  ipython-kernel = hackGet haskell/iHaskell + "/ipython-kernel";
  hasktorch-src = hackGet ./haskell/hasktorch;
  jupyterWith = hackGet ./jupyterWith;
  probtorch-src = hackGet ./python/pytorch + "/probtorch";
  pytorch-src = hackGet ./python/pytorch + "/pytorch";
  libtorch-src = hackGet ./python/pytorch + "/libtorch";

in {
  # haskell package
  inherit
    hvega
    ihaskell-hvega
    iHaskell
    ghc-parser
    ipython-kernel
    hasktorch-src
    jupyterWith
    pytorch-src
    probtorch-src
    libtorch-src;
}
