{ # we use rhyolite's obelisk in branch `develop`
  obelisk ? import .obelisk/impl (builtins.removeAttrs args ["pkgs"])
, pkgs ? obelisk.nixpkgs
, ...
} @ args:

let
  inherit (obelisk.reflex-platform) hackGet nixpkgs;
  inherit (pkgs) lib;
  hsLib = pkgs.haskell.lib;
  composeExtensions = pkgs.lib.composeExtensions;
  common = import deps/shared.nix { inherit hackGet; };
  inherit (common)
    hvega
    ihaskell-hvega
    iHaskell
    ghc-parser
    ipython-kernel
    hasktorch-src
    jupyterWith
    probtorch
    pytorch
    libtorch;
  libtorchFfiPkg = (import hasktorch-src).libtorch-ffi_cpu;
  hasktorchPkg = (import hasktorch-src).hasktorch_cpu;
  hasktorchCodegenPkg = (import hasktorch-src).hasktorch-codegen;
  overrideSrcs = {
     inherit hvega;
  };

  haskellOverrides =  lib.foldr composeExtensions obelisk.haskellOverrides [
    (self: super: lib.mapAttrs (name: path: self.callCabal2nix name path {}) overrideSrcs)
    (self: super: with hsLib;
      let
        noCheck = p: dontCheck p;
        noHaddock = p: dontHaddock p;
        fast = p: noHaddock (noCheck p);
        optionalString = nixpkgs.stdenv.lib.optionalString;
        isDarwin = nixpkgs.stdenv.isDarwin;
        callDisplayPackage = name:
              self.callCabal2nix
                "ihaskell-${name}"
                "${iHaskell}/ihaskell-display/ihaskell-${name}"
                {};
      in {
        #========== iHaskell packages ==========
        #
        # The following iHaskell setup is literally taken
        # from iHaskell github repository:
        #
        # https://github.com/gibiansky/IHaskell/blob/master/release.nix
        #
        #
        ihaskell = noHaddock (overrideCabal
          (self.callCabal2nix "ihaskell" iHaskell {})
          (old: {
            preCheck = ''
              export HOME=$(${nixpkgs.pkgs.coreutils}/bin/mktemp -d)
              export PATH=$PWD/dist/build/ihaskell:$PATH
              export GHC_PACKAGE_PATH=$PWD/dist/package.conf.inplace/:$GHC_PACKAGE_PATH
            '';
            configureFlags = (old.configureFlags or []) ++ [
              "--enable-executable-dynamic"
            ];
          })
        );
        ghc-parser = self.callCabal2nix "ghc-parser" ghc-parser {};
        ipython-kernel = self.callCabal2nix "ipython-kernel" ipython-kernel {};
        ihaskell-aeson = callDisplayPackage "aeson";
        ihaskell-blaze = callDisplayPackage "blaze";
        ihaskell-charts = callDisplayPackage "charts";
        ihaskell-diagrams = callDisplayPackage "diagrams";
        ihaskell-gnuplot = callDisplayPackage "gnuplot";
        ihaskell-graphviz = callDisplayPackage "graphviz";
        ihaskell-hatex = callDisplayPackage "hatex";
        ihaskell-juicypixels = callDisplayPackage "juicypixels";
        ihaskell-magic = callDisplayPackage "magic";
        ihaskell-plot = callDisplayPackage "plot";
        ihaskell-static-canvas = callDisplayPackage "static-canvas";
        ihaskell-widgets = callDisplayPackage "widgets";
        zeromq4-haskell = doJailbreak (fast (super.callHackage "zeromq4-haskell" "0.8.0" {}));

        #========== hvega packages ==========
        hvega = fast (self.callCabal2nix "hvega" hvega {});
        ihaskell-hvega = fast (self.callCabal2nix "ihaskell-hvega" ihaskell-hvega {});

        #========== Hasktorch packages ==========
        #
        #
        # the Following hasktorch packages attributes are taken from
        # hasktorch github repository:
        # https://github.com/hasktorch/hasktorch.git
        #
        hasktorch = fast hasktorchPkg;
        hasktorch-codegen = hasktorchCodegenPkg;
        libtorch-ffi = libtorchFfiPkg;

        #========== tensorflow package =========
        #
        # TODO:
        #   - make tensorflow package built with nix


    })
  ];

  jupyter = haskellPackages: haskellPkg: pythonPkg: pythonOverridesList:
    import ./jupyter
      { inherit jupyterWith
                haskellPackages # all haskell packages that will be available inside jupyter
                haskellPkg # all haskell packages that will be imported to iHaskell
                pythonPkg # all python packages that will be imported to iPython
                pythonOverridesList;
      };

in obelisk // {
  inherit haskellOverrides jupyter;

  project = base: projectDefinition:
    obelisk.project base ({...}@args:
      let def = projectDefinition args;
      in def // {
        overrides = composeExtensions haskellOverrides (def.overrides or (_: _: {}));
      });
  shellToolOverrides = ghc: super: {
    inherit (jupyter ghc) jupyterLabWithKernels;
  };


}
